BFIND is a dot command to assist with navigation within
NextBASIC programs, written by Chris Taylor and Simon N
Goodwin. It can find specified text within the current 
program and automatically positions the BASIC editor
cursor at the matching line.

Type 

    .bfind

for usage information, e.g:

    BASIC Find String v0.4
    bfind [@]|[@nnnn] <str>
     @     search from edit line
     @nnnn search from line nnnn
     <str> string to find

In the simplest case, we can find the first instance of the
text "Main menu" in the current program, like this:

    .bfind Main menu

If the text is not found, a 'Not Found' message is displayed.
Otherwise the matching line number is displayed and the BASIC
editor cursor is positioned there.

To search onwards through the program from after the line with
the editor cursor, specify @ between bfind and the pattern:

    .bfind @ Main menu

To search starting from a specified line, put the line number
immediately after the @ sign, e.g:

    .bfind @500 Main menu

This will start searching at line 500 till a match is found
or the end of the program is reached.

To search for a NextBASIC label, use the @ prefix as usual:

    .bfind @label

To search for a reference to that label from line 500 on, use:

    .bfind @500 @label


POINTS TO NOTE

The scan is case-independent so MAIN MENU or main menu would
also be found. Variable names, procedure names and text in
comments or strings can be found this way. Spaces are treated
as significant so MainMenu would NOT be matched this way.

Binary data within the program such as keyword tokens will 
not be found unless you use .$ to pass the appropriate token 
to the dot command, e.g:

    a$=CHR$ 236+"9999"
    .$ bfind a$

as 236 is the token for GO TO, from Appendix A of the manual.

BFIND can be used to find specific numbers within a program:

    .BFIND 23606

would find a the first literal reference to the address of the 
system variable CHARS in the program. However, be aware that
tokenisation - the differences between the stored format
of the program and the text of the listing - means that
arbitrary sub-expressions are not likely to match unless
they appear in an integer context (preceded by a % sign). 


FURTHER READING

The latest source code and documentation for BFIND is here:

https://github.com/taylorza/zxn-inlineasm-doc/blob/main/Examples/DOTCommand-bfind.md

BFIND is released under the CC-BY-NC-SA version 4 License
(https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
