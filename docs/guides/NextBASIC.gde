@title NextBASIC Guide
@author Garry Lancaster
@version 1.06
@copyright Copyright (C)2024 Garry Lancaster
@date 2024/02/12
@--index INDEX
@----------------------------------------------------------------------------
@node MAIN
@next EDITOR
@{r}NextBASIC Guide
@{c}@{h1}NextBASIC

This is a guide for the @{i}NextBASIC@{ui} editor. Information on BASIC commands
and functions may be added in the future. For now, please see the @{b}ZX Spectrum
Next User Manual@{ub} and the documentation on this SD card, in particular:

        @{b}c:/docs/nextzxos/NextBASIC_New_Commands_and_Features.pdf@{ub}
        @{b}c:/docs/nextzxos/NextBASIC_File_Commands.pdf@{ub}


@{"NextBASIC editor" LINK EDITOR}
@{"Edit options menu" LINK OPTIONS}
@{"Editing keys" LINK EDITKEYS}
@{"Editing-related commands" LINK EDITCMDS}
@{"UDGs and keyword tokens" LINK UDGS}
@{"Configuration - code options" LINK CODECFG}
@----------------------------------------------------------------------------
@node EDITOR
@prev MAIN
@next OPTIONS
@toc MAIN
@{r}NextBASIC Guide
@{c}@{h1}The NextBASIC editor

The editor shows a partial listing of the current NextBASIC program on the
screen. You can move around the listing using the arrow keys (or the special
@{"editing keys" LINK EDITKEYS}), and modify or add program lines.

The program listing shows various syntax items (keywords, line numbers etc)
in different colours depending upon the currently-selected editor colour
scheme. For more information on editor colour schemes, see the @{b}NextZXOS@{ub} guide,
found on the main menu. Note that syntax-highlighting colours are currently
only supported for 32-character editing mode. However, graphics characters can
still be distinguished in the other editing modes as they will be shown as
inverse video.

To add a new program line, move the cursor to a blank screen line (pressing
@{b}ENTER@{ub} will insert a new blank line for you after the current program line),
then type the line number followed by the commands (these are typed one letter
at a time, and do not use the "keyword" symbols shown on the Next's keyboard).

To enter a line into the program, press @{b}ENTER@{ub}. If the line is accepted
there will be an acknowledgement beep and the line will be reformatted and
displayed using the syntax-highlighting colours of the current colour scheme.
If there is an error on the line, a lower beep will sound and the cursor will
move to the location of the error so that you can correct it.

To delete an entire line, just type its number on a blank line and press
@{b}ENTER@{ub}. The line will then be removed from the listing.

To copy a line, edit its line number to a new number and press @{b}ENTER@{ub}. The
copy will be added with the new line number, leaving the original line intact.

You can run commands directly from the NextBASIC editor. To do this, simply
enter the required commands without a preceding line number.

There are also some useful @{"editing-related commands" LINK EDITCMDS} available.

Pressing @{b}EDIT@{ub} brings up a @{"menu" LINK OPTIONS} for further options.
@----------------------------------------------------------------------------
@node OPTIONS
@prev EDITOR
@next EDITKEYS
@toc MAIN
@{r}NextBASIC Guide
@{c}@{h1}Edit options menu

The options menu provides further available actions. If you bring the menu up
by mistake, press @{b}SPACE@{ub} or @{b}BREAK@{ub} to dismiss it and return to the NextBASIC
editor.

@{h2}Command Line
Switches to the @{i}Command Line@{ui}.

@{h2}32/64/85
Cycles between 32/64/85 columns-per-line mode, allowing different amounts of
information to be shown on screen. Note that syntax highlighting is currently
only available in 32-column mode.

@{h2}Screen
Switches between editing the program in the main screen or in the bottom 2
lines of the screen only. Editing in the bottom 2 lines can be useful if you
want to be able to edit your program whilst keeping the output from its last
run visible in the main screen. This option can also be toggled by pressing
@{b}EXTEND@{ub} and then @{b}EDIT@{ub}.

@{h2}Autonumber
Starts automatic program line number entry in steps of 10, beginning after the
current line.

You can change the current automatic line number by deleting it and entering a
different number; subsequent automatic numbers will follow this new number.

To stop automatic line numbering, delete the provided number and press @{b}DELETE@{ub}
one more time.

For more control over automatic line numbering, you can use the @{b}LINE NEW@{ub}
@{"editing command" LINK EDITCMDS}.

@{h2}Renumber
Renumbers the program. Lines will be renumbered in steps of 10, starting at 10.
For more control over renumbering, you can use the @{b}LINE@{ub} @{"editing command" LINK EDITCMDS}.

@{h2}Clear
If you are running very short of memory, attempting to enter a line may fail.
This option performs a @{b}CLEAR 65367@{ub} command to free up as much memory as
possible.

@{h2}Token keys
Toggles keys @{b}A S D F G Y U Q@{ub} between entering symbols (@{i}~ | \ { } [ ] @{ui}) or
keyword tokens. This option can also be toggled by pressing @{b}EXTEND@{ub} and then @{b}1@{ub}.
See the section @{"UDGs and keyword tokens" LINK UDGS} for full details.

@{h2}String tokens
Toggles whether graphics characters or keyword tokens are shown within strings.
This option can also be toggled by pressing @{b}EXTEND@{ub} and then @{b}2@{ub}. See the section
@{"UDGs and keyword tokens" LINK UDGS} for full details.

@{h2}Guide
Brings up this guide.

@{h2}Exit
Exits the editor and returns to the main NextZXOS menu.
@----------------------------------------------------------------------------
@node EDITKEYS
@prev OPTIONS
@next EDITCMDS
@toc MAIN
@{r}NextBASIC Guide
@{c}@{h1}Editing keys

The editor provides a number of special editing keys. A summary of these can
be seen at any point whilst in the editor by pressing the yellow @{b}NMI@{ub} button
on the side of your Next, selecting the @{i}Keymap@{ui} option and pressing
@{b}EXTEND@{ub}. This also shows the colours/shapes of the cursor in different
modes (for the default colour scheme).

@{h2}CAPS LOCK
Switches between capital and lower-case entry.

@{h2}GRAPH
Toggles graphics mode, for entering block graphics characters and UDGs.
See the section @{"UDGs and keyword tokens" LINK UDGS} for full details.

@{h2}EDIT
Brings up the editing options menu.

@{h2}TRUE VIDEO
Moves the cursor 1 word to the left.

@{h2}INV VIDEO
Moves the cursor 1 word to the right.

@{h2}EXTEND
Typically used as part of a special movement or delete operation (see list
below). However, this can also be used to enter many keyword tokens shown on
the keyboard. See the section @{"UDGs and keyword tokens" LINK UDGS} for full details.

@{h2}EXTEND followed by left arrow
Moves the cursor to the start of the BASIC line.

@{h2}EXTEND followed by right arrow
Moves the cursor to the end of the BASIC line.

@{h2}EXTEND followed by up arrow
Moves the cursor up by 10 screen lines.

@{h2}EXTEND followed by down arrow
Moves the cursor down by 10 screen lines.

@{h2}EXTEND followed by CAPS LOCK
Moves the cursor to the start of the program.

@{h2}EXTEND followed by GRAPH
Moves the cursor to the end of the program.

@{h2}DELETE
Deletes the character to the left of the cursor.

@{h2}EXTEND followed by DELETE
Deletes the character under the cursor.

@{h2}EXTEND followed by TRUE VIDEO
Deletes to the start of the current word.

@{h2}EXTEND followed by INV VIDEO
Deletes to the end of the current word.

@{h2}EXTEND followed by 9
Deletes to the start of the BASIC line.

@{h2}EXTEND followed by 0
Deletes to the end of the BASIC line.

@{h2}EXTEND followed by 1
Toggles the @{i}token keys@{ui} option. See the section @{"UDGs and keyword tokens" LINK UDGS} for full
details.

@{h2}EXTEND followed by 2
Toggles the @{i}string tokens@{ui} option. See the section @{"UDGs and keyword tokens" LINK UDGS} for
full details.
@----------------------------------------------------------------------------
@node EDITCMDS
@prev EDITKEYS
@next UDGS
@toc MAIN
@{r}NextBASIC Guide
@{c}@{h1}Editing-related commands

A few commands are available for manipulating NextBASIC lines. These can
usually only be run directly, and not used as a command within a program.

@{h2}LINE NEW start,step
Starts automatic program line number entry, beginning with line number @{i}start@{ui}
and incrementing by @{i}step@{ui} for each subsequent line. If @{i}start@{ui} is not
provided, new numbers start after the current editor line. If @{i}step@{ui} is not
provided, line numbers go up in steps of 10.

You can change the current automatic line number by deleting it and entering a
different number; subsequent automatic numbers will follow this new number.

To stop automatic line numbering, delete the provided number and press @{b}DELETE@{ub}
one more time.

Automatic line numbering can also be performed from the NextBASIC menu.

@{h2}LINE start,step
Renumbers the program, with the first line being numbered to @{i}start@{ui} and
subsequent lines incrementing by @{i}step@{ui}. Renumbering can also be performed
from the NextBASIC menu (but only with a start and step of 10).

@{h2}LINE m,n TO start,step
Renumbers a block of lines. Only lines between @{i}m@{ui} and @{i}n@{ui} (inclusive) will be
renumbered, with the first being renumbered (and moved, if necessary) to @{i}start@{ui}
and subsequent lines in the block incrementing by @{i}step@{ui}.

@{h2}LINE MERGE first,last
Merges all the lines in the range @{i}first@{ui} to @{i}last@{ui} (the resulting line
will have the same number as the first in the range). Care should be taken not
to merge lines forming a long-form @{b}IF..ELSE..ENDIF@{ub} structure.

@{h2}ERASE first,last
Erases the lines in the range @{i}first@{ui} to @{i}last@{ui}.

@{h2}ERASE
Erases the entire program. This command may also be included in a program, and
can be useful as the last line of an AUTOEXEC.BAS program.

@{h2}LIST target
Lists the program (and sets the current editor line) to the desired target,
which may be a line number, label, procedure or function. eg:
        @{b}LIST 200@{ub}               @{b}LIST PROC procname()@{ub}
        @{b}LIST @labelname@{ub}        @{b}LIST FN functionname()@{ub}

@{h2}BANK n LIST target
Lists the banked program section (for bank @{i}n@{ui}) from the target (which,
again, may be a line number, label, procedure or function).

@{h2}BANK n LINE first,last
Copies the range of lines specified in the main program to bank @{i}n@{ui},
replacing any program section (or other data) that was previously stored in
the bank.

@{h2}BANK n LINE first,last REM
As above, but strips out all @{b}REM@{ub} statements (including statements
starting with @{b};@{ub} ) and blank lines.

@{h2}BANK n MERGE
Merges all lines in the banked program section (in bank @{i}n@{ui}) back into the
main program.

@{h2}.RET
This dot command displays the current contents of the return stack. This is
useful when an error occurs, allowing you to see the currently-active loops,
procedures, subroutines, error handlers and local variables.

@{h2}.MEM
This dot command shows currently available main memory and memory bank
information.

@{h2}.CLEAR
This dot command deallocates all currently-allocated resources, giving a quick
way to uninstall drivers and free all memory during program development.
@----------------------------------------------------------------------------
@node UDGS
@prev EDITCMDS
@next CODECFG
@toc MAIN
@{r}NextBASIC Guide
@{c}@{h1}UDGs and keyword tokens

You can enter any graphics character or keyword token directly from the
editor keyboard.

Graphics characters and keyword tokens share the same codes in the character
set, so which is displayed depends upon the context. When typing into the
editor, these characters are always shown as graphics characters. After @{b}ENTER@{ub}
is pressed, the line is syntax-checked and accepted into the program. Keyword
tokens are then shown unless the character is part of a string, in which case
graphics characters are shown.

To show keyword tokens even if the character is part of a string, toggle the
@{i}string tokens@{ui} option from the edit menu, or press @{b}EXTEND@{ub} followed by @{b}2@{ub} to
toggle the option. This is useful for checking the contents of a string
intended for use with @{i}VAL@{ui} or @{i}VAL$@{ui}, for example. However, take care
not to edit such lines in this mode as otherwise tokens in a string will be
converted into their individual letters.

@{h2}Entering graphics characters
After pressing @{b}GRAPH@{ub}, graphics mode is entered. The available keys
are then as follows, some requiring the @{b}CAPS SHIFT@{ub} or @{b}SYMBOL SHIFT@{ub} keys
or the @{b}DRIVE@{ub} button (on the left of the case) to be held down:

@{i}Key@{ui}             @{i}Characters generated@{ui}                            @{i}ASCII codes@{ui}
9 or GRAPH      exits GRAPH mode                                   -
0 or DELETE     deletes character to the left                      -
1-8             block character graphics shown on the keys      128-135
CAPS + 1-8      inverse block character graphics                136-143
A-Z             UDGs A-Z (shown as A-Z)                         144-169
SYM + 1-8       extended UDGs (shown as 1-8)                    170-177
SYM + A-Z       extended UDGs (shown as a-z)                    178-203
DRIVE + A-Z     extended UDGs (shown as a-z, underlined)        204-229
CAPS + A-Z      extended UDGs (shown as A-Z, underlined)        230-255

@{h2}Entering tokens
Tokens shown above a key are entered in extended mode. For example, press and
release @{b}EXTEND@{ub} and then press @{b}I@{ub} to enter the code for @{i}CODE@{ui}. Or press and
release @{b}EXTEND@{ub}, then hold @{b}SYMBOL SHIFT@{ub} and press @{b}I@{ub} to enter the code for @{i}IN@{ui}.

Tokens shown on the top row on a key must be entered in graphics mode, with
@{b}CAPS SHIFT@{ub} held down. For example, press @{b}GRAPH@{ub} to enter graphics mode
and then hold @{b}CAPS SHIFT@{ub} and press @{b}I@{ub} to enter the code for @{i}INPUT@{ui}. Press @{b}GRAPH@{ub}
again to leave graphics mode.

Tokens shown on the 2nd row on a key are entered in normal mode using @{b}SYMBOL
SHIFT@{ub}. For example, holding @{b}SYMBOL SHIFT@{ub} and pressing @{b}I@{ub} enters the code
for @{i}AT@{ui}. Note that this is overridden on some keys (eg @{b}A S D F G Y U Q@{ub}) to
give easier access to other symbols. To enter tokens instead for these keys,
toggle the @{i}token keys@{ui} option from the edit menu, or press @{b}EXTEND@{ub} followed
by @{b}1@{ub} to toggle the option.

@{h2}Default token options
The @{i}token keys@{ui} option is controlled by bit 7 of the @{i}FLAGS2@{ui} system
variable. You can therefore change the default to enter tokens instead of
symbols with a line such as the following in your @{i}AUTOEXEC.BAS@{ui}:
  @{b}10 POKE %$5c6a,%PEEK $5c6a|$80@{ub}

Similarly, the @{i}string tokens@{ui} option is controlled by bit 7 of the @{i}TV_FLAG@{ui}
system variable. It is not recommended to set this by default, however.
@----------------------------------------------------------------------------
@node CODECFG
@prev UDGS
@toc MAIN
@{r}NextBASIC Guide
@{c}@{h1}Configuration - code options

The behaviour of some NextBASIC features can be configured using the special
@{b}%CODE@{ub} integer variable.

This is a 16-bit variable, with each bit controlling a particular behaviour.
This variable is reset to its default value of 0 whenever a program is @{b}LOADed@{ub}
or @{b}RUN@{ub}. Therefore, a program should set any desired options when it starts.

@{h2}%CODE bits
The following behaviour is controlled for each bit in @{b}%CODE@{ub}:

        @{b}Bit@{ub}     @{b}Behaviour@{ub}
        0       If set, @{b}%RND n@{ub} and @{b}RND(n)@{ub} return values in the range 0..n
                If reset, they return values in the range 0..n-1 (0 if @{b}n@{ub}=0)
        1       If set, the @{b}BREAK@{ub} key is disabled
        2       If set, @{b}DEF FN@{ub} statements entered will be treated as
                legacy 48K BASIC functions (allowing DEFADD parameter parsing,
                but not allowing recursion, long parameter names, initialisers
                etc)

Other bits are reserved for future use and should be left as zeroes.

@{h2}Example
@{b}10 %CODE=BIN 10@{ub}
Disables the @{b}BREAK@{ub} key whilst the program is running.
@----------------------------------------------------------------------------
@node TEMPLATE
@toc MAIN
@{r}NextBASIC Guide
@{c}@{h1}


@----------------------------------------------------------------------------
@node INDEX
@toc MAIN
@{r}NextBASIC Index
