NextPi Hardware Interfaces
--------------------------

There is no hardware connectivity between the Next and the Pi outside protocols
documented below, and no other existing hardware control points exist between
the two devices at this time.  While is is possible that future connections
between the two endpoints could be created, DO NOT presume this is the case as
the FPGA on the original KS1 Nexts is very close to capacity already.

You can build your own custom communications protocol, but that is left as an
exercise to the developer. "No hardware link outside of these connections"
between the two systems means no direct memory, no physical device access,
etc. No integrated instructions on the CPU, or hardware beyond that mentioned
below.

That said, a developer could build a routine over GPIO to sit on the Pi side,
and treat the Pi as a "Copper", or to handle large memory transfers, etc. but
they need to start from "foundations" for all that.


UART
====
The UART currently runs at 115,000 and is paired to a BASH Supervisor shell,
running at ROOT, with a preconfigured path to support the default CLI override
system.  This is how Pisend, Piput, Piget, etc. (current NextZXOS integrations)
all work.  File-transfers and "terminal commands" between the pi and Next are
currently handled via this interface.

I2S DAC
=======
The ALSA stack is configured to emulate a TOSLink interface, sending digital
PCMdata to the Next for it to decode upon its own DAC natively.  This means 
that any API that natively supports ALSA on linux gets access to the next output
DAC. Because this DAC is a "pure digital" solution, with no DSP attached, volume
control needs to be done at the sample generation/output level, upon the pi, or
likewise upon the next itself - there is no "volume control" or "amp" between
the two systems.  Audio players, including TZX loading, are currently handled
via this interface.

GPIO
====
The Next's own GPIO lines are connected to the Pi header, and therefore permit
the creation of custom GPIO based protocols for communication and control. This
isn't currently used by any included applications in NextZXOS, but has been
proven to be a practical method for those who wish to adopt it.


