!� �:��:�C��C��C�C�~#�(�!8�	h� ����* �4W �9���C���  B~��#�!8��0��
���)DM))	�1��~��#�Terminal resize utility

Size can be up to 32x80 (defaults to 24x80, suitable for many programs)
If setting a reduced size, the top & left parameters can be used to make
the image more centred on your screen

Usage: TERMSIZE top left height width
   eg: TERMSIZE 0 0 32 80
$